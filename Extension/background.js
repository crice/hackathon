//Called when user clicks on plugins'
var get_carbon_footprint = function(title) {

	var footprint = Math.random() * 1000;
	footprint = Math.trunc(footprint);

	for (var i = 0; i < title.length; i++)
		footprint += title.charCodeAt(i);

	footprint %= 13046; //proprietary algorithm
	footprint %= 1000;

	return footprint;
};

if (document.title.indexOf("Amazon") != -1){
	if(document.title.localeCompare("Amazon.com Shopping Cart") == 0){
			//var myItems = document.querySelectorAll("div.a-row.a-size-small")
			var realCheckout = document.getElementById("activeCartViewForm");
			var b = document.createElement("BUTTON");		
			b.innerHTML = "FAKE CHECKOUT"; 
			realCheckout.appendChild(b)	

			var myItems = document.getElementsByClassName("a-size-medium sc-product-title a-text-bold")
			var i, itemName, fpStr = "Carbon footprint: HIGH"
			for(i = 0; i < myItems.length; i++){
				var t = document.createTextNode(fpStr);
				var d = document.createElement("div")
				d.appendChild(t)		
				itemName = myItems[i].innerHTML
				myItems[i].parentNode.insertBefore(d, myItems[i].siblingBelow)
			}
		}
	else{		
		var myItems = document.querySelectorAll("div.a-row.a-size-small:not(.a-color-secondary");
		var myTitles = document.querySelectorAll("span.a-size-medium.a-color-base.a-text-normal, span.a-size-base-plus.a-color-base.a-text-normal");
		myItems.forEach(function(node, index){
			var myImage = new Image(20,20);
			//todo replace node.title with actual product title
			var footprint = get_carbon_footprint(myTitles[index].innerText);
			footprint = Math.trunc(footprint);
			var level;
			if (footprint < 20){
				level = "Very Low";
				myImage.src = chrome.runtime.getURL("Earth-Chill.png");
			}
			else if (footprint < 50){
				level = "Low";
				myImage.src = chrome.runtime.getURL("Earth-Chill-2.png");
			}
			else if (footprint < 100){
				level = "Moderate";
				myImage.src = chrome.runtime.getURL("Earth-Chill-3.png");
			}
			else if (footprint < 250){
				level = "Average";
				myImage.src = chrome.runtime.getURL("Earth.png");
			}
			else if (footprint < 450){
				level = "High";
				myImage.src = chrome.runtime.getURL("Earth-2.png");
			}
			else if (footprint < 700){
				level = "Very High";
				myImage.src = chrome.runtime.getURL("Earth-3.png");
			}else{
				level = "Extremely High";
				myImage.src = chrome.runtime.getURL("Earth-Burning.png");
			}
			var newLine = document.createElement("div");
			var t = document.createTextNode("Carbon footprint: " + footprint + " lb CO2(" + level + ")");
			newLine.appendChild(myImage);
			t.className = "a-row";
	//		t.setAttribute("style", "background: linear-gradient(green, blue)");
			newLine.appendChild(t);
			node.appendChild(newLine);
		});
		//todo remove
		console.log(myItems);
		console.log(myTitles);
	}
}
